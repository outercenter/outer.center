---
title: Ontogen
layout: post
tags:
- poem
date: '2020-09-09 15:48:16'
---

<div class="poem">
	Adrift in the tundra of emptying transit
	The man from the meadow reflects on the habit
	He finds in the shiver of rivers of ozone
	The stink of the metal and cowflesh of his home.
	
	Intrigued by a friend in the sentinel spreading
	The ape who'd escaped from perpetual treading
	He finds in the crack of the black of his shadow
	The musk of old dust that implies he's a rascal.
	
	Basking the open of closing out lifelines
	The reptile instant delights in the nighttime
	He finds in the freckle of skin universal
	The cue that can slither without a rehearsal.
	
	Driven by bubbles that slide in environs
	The fish in the soup refusing the silence
	He finds in the middle of scale of outside
	The slime embryonic explicitly can't hide.
	
	Splitting to prove its the whole and the wholesome
	The germ from the babble who blathers and then some
	He finds in the nothing of space all about him
	The mystery's purpose is drowning the fountain.
	</div>
