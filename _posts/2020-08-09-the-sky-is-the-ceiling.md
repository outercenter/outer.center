---
title: The Sky is the Ceiling
layout: post
tags:
- poem
date: '2020-08-09 12:28:34'
---

<div class="poem">
						
						At red lights the zen method whips to a frenzy
						And static is captured from light in the air
						So daylong the minutes go flat with disaster
						And Gauguin and so long and Godspeed and no more
						Explode into rancor that vaguely complies.
						
						He opens the car door and calmly just wanders
						In traffic and headlong at tail-end elated,
						A nomad of so what that shuts up at shallows
						To reel that he's feeling the thrill it implies.
						
						But really he doesn't in drumming the doldrums
						That care to impair the indignities manic,
						And shouting at windshields and bumpers and daylight
						He drives off in limits that don't drag him down.
						</div>
