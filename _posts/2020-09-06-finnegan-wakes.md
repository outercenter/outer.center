---
title: Finnegan Wakes
layout: post
tags:
- poem
date: '2020-09-06 11:05:20'
---

<div class="poem">
	The sea of sway that's free today to seem to say
	In need indeed is seed decreed defeat is in the way,
	The way whereof there is no will so dying now can only fill
	A life of vice with eager waste to take from shame the peopled place,
	The place that winds as though it finds its path at last without the signs
	That point for reasons unanointed to the moving other lines,
	The lines of some addition wishing process marks a single grace
	For lonesome perfect little order showing off its little face,
	The face that watches time the spiral die as though confined in glass
	Yet thick without the shattered match of shadows on the sunny sea.
	</div>
