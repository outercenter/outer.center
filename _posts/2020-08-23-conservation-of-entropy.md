---
title: Conservation of Entropy
layout: post
tags:
- poem
date: '2020-08-23 13:26:47'
---

<div class="poem">
	The love songs of starmen will far on go quiet
	For dimensional soulmates of manifold daytime
	Where-when the sunset is depth everlasting
	In causal unwinding that undoes a vow.
	
	How then to know that to keep up with tautness
	Is difference in codex as conscience in choice
	Who-why the truth isn't tables and chairlegs
	In awful defining that once was as now?
	
	What is a question for stacking up brick blocks,
	But not as the pieces lose meaning in order,
	But not as a labyrinth is duller with time.
	</div>
