---
title: Volume Itself
layout: post
tags:
- poem
date: '2020-08-05 07:37:45'
---

<div class="poem">
	As specious as spectres detect in spaces
	As flat as the last of the start of descending,
	The speed of exceeding depletes in its paces
	The passage of panics detached from the ending.
	
	An intangible mass is attached as adornment
	To central encompass of balance unfolding,
	So the friction of self-parts enhances the halting
	That matches in scaling the scope of discernment.
	
	I'd idly endeavor in inking identity
	To borrow the pleasure of lonesome impatience
	Of wave-breaks unnascent on shorelines unending
	That wilfully fill up to cough out the nothing.
	
	Voiced in the noise of the name of behaviors
	Is limit of loving the drifting applause
	As anxious as pavement for cracking and crushing
	In rush to discover distaste and betrayers.

	A clock is a torus of snaking to famine
	That pangs to exclaim it's unique simultaneous
	But refrains from repeating the deed of the signpost
	That fell down before the roadway was paved.
	</div>
