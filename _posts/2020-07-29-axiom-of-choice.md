---
title: Autocorrelation / Axiom of Choice
layout: post
tags:
- poem
date: '2020-07-29 10:20:30'
---

<div class="poem">
	There are as many relations as
	Can't be counted that
	Add up to answers that
	Wanted for finding but
	Didn't mean much since
	Just on the edge of
	An infinitesimal is
	An outcome that wasn't
	What needed to be.
	
.
	
	To speak of induction from womanly manhood
	Is treat to deceive that grouping is reason
	For carried varieties brimming with edgeness
	So far as the during is over at once.
	
	To defeat with deduction from masculine hysteresis
	Will twice need to bracket the answer of visage
	But not for the purpose of purpose imploring
	That fuses fugacious can last us the while.
	
	Beyond the distinction is spectrum of clarity
	That noisily engenders the meat metaphysical
	As replete and inimical to sheath of deliverance
	That bleeds when it seeks to the heart of the thread.
	
	</div>
