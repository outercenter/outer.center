---
title: An Exercise to the Reader / Up to Isomorphism
layout: post
tags:
- poem
date: '2020-08-30 06:38:26'
---

<div class="poem">
	In premise the premise should follow as stated
	Which may be what makes it a perfect solution
	To claim the immaculate enpostulated
  And maybe be better than what it can be.
	
	There's a measure that matters and much more that doesn't
	But doesn't that mean that to mean what you wanted
	You need to assume that between me and you in our object relation
	The object of asking the question and guessing
	Is best left to saying it's true if it's true.
	
	-
	
	Is any of this real
	Or is a computer giving a test
	I am failing?
	
	If evil is banal
	The mundane should undo
	The taboo of praying
	And let slip a bad thought
	Or worse yet the doubtful
	That tangles with tinctures
	The vinculated charmed.
	
	When the mirror shows pictures
	That aren't in the picture
	What am I expected to do?
	
	If ignorance blisters at knowing uneasy
	Then throw away grieving of pieces and sums
	In favor of savoring profligate saviors
	Patched up with sheerness and pressed hard to run.
	
	Thus in losing its players
	The game takes their places
	And gives one to me for a turn.
	</div>
