---
title: One-thousand of Words
layout: post
date: '2020-07-18 11:36:02'
tags:
- poem
---

<div class="poem">
Hidden in pictures is actually happening
All of the instants the image relates
Like food and some footfalls at dusk
On a blanket at rest in the grass with a tree.

The shape of the apex is namely as stated,
Related in features of orchid behaviors,
Pretending at ashes and dust to themselves
And ending in state that improves on before.

All of your life could be put down in few words
But rolled out to a thickness of hot satisfaction
That there in the image of words on a paper
The things took their places as well as they could.

With heady indifference the body is carried
From palpitant city to streets wide alone,
Standing of symbols that stand up to sit down
And listen to beauty and not sing along.
</div>
