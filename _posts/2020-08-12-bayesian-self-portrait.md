---
title: Bayesian Self-Portrait
layout: post
tags:
- poem
date: '2020-08-12 15:57:33'
---

<div class="poem">
	It's not in an instance statistics exist,
	That's not what the image implicit predicts.
	The thing is as noisy as drunken mistaking
	That sings with the boys of love for the taking,
	Then slinks home awhisper in creaking the stairset
	And thinks to deliver by speaking the careless.
	In day and by day it just fidgets with madness
	In way and new way it must visit in antics,
	But watch it with gossip from rooftop unnoticed
	And posit its habits come often to focus.
	The magic of fractions is part is a sickness
	That tragic is standing in art as a witness
	To whole that can grow just as quick as the year is
	In soul that explodes just as big as the fearless.
	</div>
