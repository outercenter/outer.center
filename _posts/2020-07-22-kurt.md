---
title: Kurt
layout: post
date: '2020-07-22 18:13:49'
tags:
- poem
---

<div class="poem">
Gödel completed the search for the secret
Of sources that hid in the signal of time
Only to find that the final provisions
Would starve him of vigor in trusting divine,
The irony being his consistently seeing
The absence of matches in passing for muster
Would rob of the magic the pleasure of knowing
The act was a trick we ignored all along.

Until in his building he felt it imperious
To treat it as serious that series of statements
Related in context would come to forget
What they meant in the end,
Then relegated to specialist diet
He died from the kind of mistrust defiant
That signs on the line of agreeing in premise
But furtively thinks there's no meat in the sentence.
</div>
