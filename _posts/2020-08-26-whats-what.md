---
title: What's What
layout: post
tags:
- poem
date: '2020-08-26 18:53:23'
---

<div class="poem">
	So what when serpents have heard us they coil up
	To dry by the time that the desert is falling
	Into the night light of daytime eroding
	With glaciers unwinding at finding the crux.
	
	Then what encumbers the mantle to ravel
	From gravel the planet to spin up its spirit
	As near as the season is pleasing to stroll in
	In little but living the image as such.
	
	For what is given the timorous whistle
	To tremble in embers the size of the wilds
	That grind from the marrow the malleable moment
	And freeze it completely in seeds come apart.
	
	And what emotion can cry in the cupboard
	To lie to a husband in saucerful curtsies
	Out of the slaving affection of tyrants
	As waiting containment to shut up and start.
	</div>
