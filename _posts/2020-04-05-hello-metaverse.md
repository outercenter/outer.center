---
title: Hello, Metaverse.
layout: post
date: '2020-04-05 13:32:14'
tags:
- meta
- absolute relativism
---

And welcome.

What is this place, and why have you come here? Consider this a respite from indirect directness in pretense of extending a hand that will come to let go. And down you will fall as confused as when starting but certain you've been taken, which we'll assume as a given. But, no, nothing to sell you and no one to whom you will be sold with no notice. Here is some thinking that thinks that to think is to think of another way.

In a word, `paradox`. In time, we will see how reconciling contradiction reveals a life meandering and meaningful wherein the irony of insolubility is the serendipitous solution to an infinity of problems that may or may not exist. In abstraction, we will feel how concreteness is a property to be prodded and poked at and marveled over for practical purposes like profit and pride. 

Our assumptions are few and our methods are scrutiny, though our efforts have errors we must needs abide. That said, we will speak in many voices to answer some questions that may at first appear unrelated until further analysis unravels that math, statistics, science, technology, language, finance, culture, computing, history, politics, poetry, and so many subsets share structures so inveterate and so vestigial that the colors of noise will form a rainbow of signal from which to pinpoint the size of the sun.

Moreover, the less our best guesses tend to the universal, the clearer it is what is clear or coherent; sometimes truth is a function of time, and sometimes it is not. What is expected is that following the rules as laid out before us, we at most expect to repeat old mistakes. The will then is to wring from these lessons the lessons within them and to make up a system like building the staircase with each step that we climb.

Now don't let this spiral of speaking persuade (or worse yet convince) you the answers are perfect or worthy or better. Much that has happened holds beauty we respect and admire, and even more has simply not happened. But enough with these word games that hint at the substance and tease uncontented the reader we hope will believe what we mean.

So welcome once more to the edge of the heart of the matter. Welcome at last to the outer center.
<div class="flex-center">{% include metaverse.svg %}</div>
