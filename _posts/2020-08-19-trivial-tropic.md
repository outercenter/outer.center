---
title: Trivial Tropic
layout: post
tags:
- poem
date: '2020-08-19 15:40:16'
---

<div class="poem">
	Common illusion gives grace to the tasteless and papers our parlors with old country fervor,
	So gratefully patient the saints are illumined to share with some peanuts the Pentecost phrase.
	The point is pretension is tending to stressors that rob the ungodly of parallel pleasures
	But don't overflavor the love of our neighbor or lusting for virture as often is thought.
	
	Here is it clear that a ditty is swansong for taught operettas exhaled in a timebomb,
	But irony has it that manic computers can sit at the ankles of pliable genius
	And not once come near to the integrand image, so prefects perfected the multiple first
	While beauty was dried out and cracking with thirst.
	
	Convenient to see in the canon unmoving, that's how they need it ergodic at rest.
	Though fair is to be in a practice of preaching to savor behaviors of outward disgust
	And feel at the heels of hinting entendre to love the displeasure pretending at praise.
	In sum, it is affine with constant to stand on, so choose one to pander to summit as base.
	</div>
