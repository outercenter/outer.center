n_rings = 108
origin_radius=300
inverse_phi = 1./1.6180339887
radius = origin_radius
diameter=2*radius
previous_stroke_width, stroke_width = 1, 1
with open('logo.svg', 'w') as f:
    # f.write(f'<svg height="{diameter}" width="{diameter}">')
    f.write(f'<svg viewBox="0 0 {diameter} {diameter}" preserveAspectRatio="xMinYMin meet" class="heartofthematter">')
    for ring in range(n_rings):
        f.write(f'<circle cx="{origin_radius}" cy="{origin_radius}" r="{radius}" stroke="#FFD7AF" stroke-width="{stroke_width}" fill="none" />')
        previous_stroke_width, stroke_width = stroke_width, (stroke_width + previous_stroke_width)*inverse_phi
        radius -= stroke_width
        if radius < 0:
            print(ring)
            break
    f.write('</svg>')
