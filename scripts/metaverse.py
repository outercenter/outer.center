import drawSvg as draw
import numpy as np

color = '#FFD7AF'

l = 512
d = draw.Drawing(l+8, l/8, origin=(0, 0))
n_rect = 24
for rect in range(n_rect):
    d.append(draw.Rectangle(rect**2, (rect**1.2)/2, rect**1.1, rect**1.1, stroke_width=1, stroke=color, fill="none"))
d.setPixelScale(2)
d.saveSvg('/draw/metaverse.svg')
print('saved')
